package com.example.pavlovka.testbarcode;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

    private MainActivityBarcodeReceiver barcodeReceiver =null;
    private TextView barcodeTextView=null;

    @Override
    protected void onResume() {
        super.onResume();

        this.registerReceiver(barcodeReceiver, new IntentFilter(APPConstants.ACTION_BARCODE_DATA));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barcodeReceiver = new MainActivityBarcodeReceiver();
        this.barcodeTextView=this.findViewById(R.id.barcodeTextView);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(barcodeReceiver);
    }

    private class MainActivityBarcodeReceiver extends CipherLabBarcodeReceiver{

        public MainActivityBarcodeReceiver() {
            super();
        }

        @Override
        public void onGetBarcodeData(String barcodeData, String barcodeType) {
            barcodeTextView.setText(barcodeData);
        }
    }


}
