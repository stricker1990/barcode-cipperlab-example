package com.example.pavlovka.testbarcode;

import android.content.Intent;

public abstract class CipherLabBarcodeReceiver extends BarcodeReceiver {

    public final String DECODER_DATA="Decoder_Data";
    public final String DECODER_CODETYPE_STRING="Decoder_CodeType_String";

    public CipherLabBarcodeReceiver() {
        super();
    }

    private String getSplitSymbole(){
        return ";";
    }

    @Override
    public boolean isBarcodeAction(Intent intent) {
        return APPConstants.ACTION_BARCODE_DATA.equals(intent.getAction());
    }

    @Override
    public String getBarcode(Intent intent) {
        String rawBarcode=intent.getStringExtra(DECODER_DATA);
        String[] substrings=rawBarcode.split(getSplitSymbole());
        switch (substrings.length){
            case 3:
                return substrings[1];
            case 2:
                return substrings[0];
            default:
                return rawBarcode;

        }
    }

    @Override
    public String getBarcodeType(Intent intent) {
        return intent.getStringExtra(DECODER_CODETYPE_STRING);
    }
}
