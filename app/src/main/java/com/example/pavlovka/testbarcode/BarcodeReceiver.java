package com.example.pavlovka.testbarcode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public abstract class BarcodeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(this.isBarcodeAction(intent)) {

            String barcodeData=this.getBarcode(intent);
            String barcodeType=this.getBarcodeType(intent);

            this.onGetBarcodeData(barcodeData, barcodeType);
        }

    }

    public abstract String getBarcode(Intent intent);

    public abstract String getBarcodeType(Intent intent);

    public abstract boolean isBarcodeAction(Intent intent);

    public abstract void onGetBarcodeData(String barcodeData, String barcodeType);
}
